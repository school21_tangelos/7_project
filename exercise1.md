***Задание: Описать алгоритмы балансировки нагрузки, указав их особенности и недостатки.***

Для решения проблемы высоких нагрузок сегодня чаще всего используется кластеризация: несколько серверов объединяются в кластер; нагрузка между ними распределяется при помощи комплекса специальных методов, называемых балансировкой. Зачастую это еще и резервирование серверов друг на друга.

Процедура балансировки осуществляется при помощи целого комплекса алгоритмов и методов, соответствующим следующим уровням модели OSI (Сетевая модель OSI — сетевая модель стека сетевых протоколов OSI/ISO. Посредством данной модели различные сетевые устройства могут взаимодействовать друг с другом):

-   сетевому;
-   транспортному;
-   прикладному.

**Алгоритм балансировки нагрузки** – это набор правил, которым следует балансировщик нагрузки для определения наилучшего сервера для каждого из различных клиентских запросов. Алгоритмы балансировки нагрузки делятся на две основные категории.

**_Статическая балансировка нагрузки_**

Алгоритмы статической балансировки нагрузки следуют фиксированным правилам и не зависят от текущего состояния сервера, он не принимает во внимание состояние системы для распределения задач. Вместо этого заранее делаются предположения для всей системы, такие как время прибытия и требования к ресурсам для входящих задач. Кроме того, известно количество процессоров, их мощность и скорость передачи данных. Следовательно, статическая балансировка нагрузки направлена ​​на то, чтобы связать известный набор задач с доступными процессорами, чтобы минимизировать определенную функцию производительности. Уловка заключается в самой концепции этой функции производительности.

Методы статической балансировки нагрузки обычно централизованы вокруг маршрутизатора, который распределяет нагрузку и оптимизирует функцию производительности. Эта минимизация может учитывать информацию, относящуюся к задачам, которые должны быть распределены, и определять ожидаемое время выполнения.

Преимущество статических алгоритмов заключается в том, что их легко настроить и они чрезвычайно эффективны в случае довольно обычных задач (например, обработка HTTP-запросов с веб-сайта). Однако по-прежнему существует некоторая статистическая разница в распределении задач, которая может привести к перегрузке некоторых вычислительных устройств.

**_Динамическая балансировка нагрузки_**

Алгоритмы динамической балансировки нагрузки проверяют текущее состояние серверов перед распределением трафика. Они учитывают текущую нагрузку каждого из вычислительных блоков (также называемых узлами) в системе. При таком подходе задачи можно динамически перемещать с перегруженного узла на недогруженный, чтобы получить более быструю обработку. Хотя эти алгоритмы намного сложнее разработать, они могут дать отличные результаты, в частности, когда время выполнения сильно варьируется от одной задачи к другой.

При динамической балансировке нагрузки архитектура может быть более модульной, поскольку не обязательно иметь конкретный узел, выделенный для распределения работы. Когда задачи однозначно назначаются процессору в соответствии с его состоянием в данный момент, это уникальное назначение. Очевидно, что алгоритм балансировки нагрузки, который требует слишком много коммуникаций для принятия решений, рискует замедлить решение общей проблемы.

#### К статистическим методам относятся:

### Метод круговой системы (Round Robin)

Метод представляет собой перебор по круговому циклу: первый запрос передаётся одному серверу, затем следующий запрос передаётся другому и так до достижения последнего сервера, а затем всё начинается сначала

В циклическом методе балансировку нагрузки выполняет авторитетный сервер имен вместо специализированного аппаратного или программного обеспечения. Сервер имен возвращает IP-адреса разных серверов в ферме поочередно или по круговой системе.

Плюсы:

Независимость от протокола высокого уровня. Для работы по алгоритму Round Robin используется любой протокол, в котором обращение к серверу идёт по имени.  
Балансировка никак не зависит от нагрузки на сервер: кэширующие DNS-серверы помогут справиться с любым наплывом клиентов.

Не требует связи между серверами, поэтому может использоваться как для локальной, так и для глобальной балансировки,.  
Низкая стоимость: чтобы они начали работать, достаточно просто добавить несколько записей в DNS.

Минусы:

Чтобы распределение нагрузки отвечало критериями справедливости и эффективности, нужно, чтобы у каждого сервера был в наличии одинаковый набор ресурсов.

При выполнении всех операций также должно быть задействовано одинаковое количество ресурсов. В реальной практике эти условия в большинстве случаев оказываются невыполнимыми.

Также при балансировке по этому алгоритму совершенно не учитывается загруженность того или иного сервера в составе кластера. Поэтому сфера применения алгоритма весьма ограничена.

Не решена проблема недоступного узла, т.е. узла, который во время эксплуатации системы внезапно вышел из строя

### Взвешенный циклический метод (Weighted Round Robin)

При взвешенной циклической балансировке нагрузки вы можете назначать разные веса каждому серверу в зависимости от их приоритета, емкости или производительности. Серверы с большим весом будут получать больше входящего трафика приложений от сервера имен.

Это — усовершенствованная версия алгоритма круговой системы.

Плюсы:

Это помогает распределять нагрузку более гибко: серверы с большим весом обрабатывают больше запросов. Т.е. удалось компенсировать возможные технические отличия узлов сети

Минусы:

Всех проблем с отказоустойчивостью не решает. Если узел сети с высоким приоритетом перестает быть доступным, на него все равно отправляется большинство запросов. Более эффективную балансировку обеспечивают другие методы, в которых при планировании и распределении нагрузки учитывается большее количество параметров.

В нём совершенно не учитывается количество активных на данный момент подключений.

### Метод хеширования IP (Destination Hash Scheduling и Source Hash Scheduling)

В методе хеширования IP балансировщик нагрузки выполняет математические вычисления, называемые хэшированием, на IP-адресе клиента. Он преобразует IP-адрес клиента в число, которое затем сопоставляется с отдельными серверами. В этом алгоритме сервер, обрабатывающий запрос, выбирается из статической таблицы по IP-адресу получателя.

Алгоритм хеширования определяет, куда распределять запросы, на основе назначенного ключа, такого как IP-адрес клиента, номер порта или URL-адрес запроса. Метод Hash используется для приложений, которые полагаются на сохраненную информацию о пользователях, например, тележки на веб-сайтах интернет магазинов.

Метод создан для работы с кластером кэширующих прокси-серверов, но он часто используется и в других случаях.

#### К динамическим методам относятся:

### Случайный выбор.

Простейшей стратегией балансировки нагрузки является случайный выбор. Используя данный подход в самом простом случае нет нужды собирать данные о нагрузке системы и состояниях компонентов. Определение узла, который получит задание на обработку запроса, производится на основе случайного выбора. Преимуществом данного алгоритма является простота его реализации и низкая нагрузка на систему балансировки, но ценность данных преимуществ сомнительна с учетом недостатка в виде распределения нагрузки без учета информации о загруженности узлов, что может являться критичным и привести к серьезным последствиям, в виде частичного или полного отказа системы. Случайная стратегия дает неплохие результаты относительно систем, не использующих балансировку, и часто используется в качестве отправной точки для сравнения других алгоритмов балансировки нагрузки

###   Метод наименьшего количества подключений или наименьшей нагрузки (Least Connections)

Соединение – это открытый канал связи между клиентом и сервером. Когда клиент отправляет первый запрос на сервер, он аутентифицируется и устанавливает активное соединение между собой. При использовании метода наименьшего количества подключений балансировщик нагрузки проверяет, какие серверы имеют наименьшее количество активных подключений, и направляет трафик на эти серверы. Этот метод предполагает, что все соединения требуют одинаковой вычислительной мощности для всех серверов.

Метод учитывает количество подключений, поддерживаемых серверами в текущий момент времени. Каждый следующий вопрос передаётся серверу с наименьшим количеством активных подключений.

Алгоритмы наименьшего количества подключений предполагают, что некоторые серверы могут обрабатывать больше активных подключений, чем другие. Таким образом, каждому серверу можно назначить разные веса или емкости, а балансировщик нагрузки отправляет новые клиентские запросы на сервер с наименьшим количеством подключений по емкости.

 Существует усовершенствованный вариант этого алгоритма, предназначенный в первую очередь для использования в кластерах, состоящих из серверов с разными техническими характеристиками и разной производительностью. Он называется **Weighted Least Connections** и учитывает при распределении нагрузки не только количество активных подключений, но и весовой коэффициент серверов.

Данный алгоритм удачно подходит для систем, в которых нагрузка в большей степени определяется количеством выполняемых заданий.

В  числе  других  усовершенствованных  вариантов  алгоритма Least Connections следует  прежде  всего  выделить

**Locality-Based Least Connection Scheduling**
 и
**Locality-Based Least Connection Scheduling with Replication Scheduling**.

### Sticky Sessions (закрепление пользовательских сессий)**

Алгоритм распределения входящих запросов,  
при котором соединения передаются на один и тот же сервер группы.  
Он  используется,  например,  в  веб-сервере  Nginx.  Сессии  пользователя  могут  быть  закреплены  за  конкретным  сервером  с  
помощью  метода  IP  hash.  С  помощью  этого  метода  запросы  распределяются  по  серверам  на  основе  IP-aдреса  клиента.  Как  указано в документации, «метод гарантирует, что запросы одного и  того же клиента будет передаваться на один и тот же сервер». Если  закреплённый  за  конкретным  адресом  сервер  недоступен,  запрос  будет перенаправлен на другой сервер. Применение этого метода сопряжено с некоторыми проблемами. Проблемы  с  привязкой  сессий  могут  возникнуть,  если  клиент  использует динамический IP. В ситуации, когда большое количество  запросов проходит через один прокси-сервер, балансировку вряд ли  
можно назвать эффективной и справедливой.

### Метод наименьшего времени отклика

Время ответа – это общее время, затрачиваемое сервером на обработку входящих запросов и отправку ответа. Скорость отклика показывает, насколько загружен сервер. Метод наименьшего времени отклика сочетает время отклика сервера и активные соединения для определения лучшего сервера. Балансировщики нагрузки используют этот алгоритм для обеспечения более быстрого обслуживания всех пользователей.

### Метод на основе ресурсов

В методе на основе ресурсов балансировщики нагрузки распределяют трафик, анализируя текущую нагрузку на сервер. Специализированное программное обеспечение, называемое агентом, работает на каждом сервере и рассчитывает использование ресурсов сервера, таких как вычислительная мощность и память. Затем балансировщик нагрузки проверяет агент на наличие достаточного количества свободных ресурсов перед распределением трафика на этот сервер.

### Зондирование.

Существует 3 стратегии, которые работают в рамках данного подхода: стратегия **предельного значения**, **жадная** стратегия и **кратчайшая** стратегия. В рамках стратегии предельного значения выбирается случайный узел, оценивается уровень нагрузки при передаче и исполнении задания, на этом узле, в случае, когда такая нагрузка оказывается выше порогового значения выбирается следующий узел. Если за определенное количество шагов целевой узел не был найден, то задание выполняется локально. Использование данной стратегии позволяет достичь значительного прироста производительности по сравнению со случайной стратегией. Жадная стратегия является разновидностью пороговой стратегии, в свою очередь использует циклический подход, при зондировании узлов. При использовании кратчайшей стратегии выбирается случайное подмножество узлов, в данном подмножестве определяется наименее загруженный узел. Если длина очереди обработки на таком узле меньше, чем определенный порог, то работа передается, иначе она выполняется локально. Значительного прироста в отношении стратегии предельного значения такая стратегия не дает

### Существуют алгоритмы балансировки, основанные на концепции переговоров.

В стратегии переговоров нагруженный узел инициирует запрос-заявку, в которой описывает свою нагрузку, а также задания, которые он хотел бы передать. Удаленный узел проверяет данный запрос и сравнивает нагрузку узла инициатора со своей, в случае если она меньше, этот удаленный узел отправляет заявку на исполнение задания инициатору. Иначе узел игнорирует сообщение. В итоге узел-инициатор получает заявки на принятие заданий и выбирает из них самые незагруженные узлы. Проблемой данного подхода может быть ситуация, в которой узлы с меньшей загрузкой могут стать перегруженными в результате победы во многих одновременных переговорах. Избавиться от этой проблемы можно, используя ограничение на количество заявок
